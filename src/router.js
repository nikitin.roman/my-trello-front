import VueRouter from "vue-router";
import store from "@/store.js";

const routes = [
  { path: "/", component: () => import("@/pages/MainPage") },
  {
    path: "/board/:board_id?/:id?",
    component: () => import("@/pages/Board"),
    meta: {
      requiresAuth: true,
    },
  },
  { path: "/auth", component: () => import("@/pages/Authorization") },
];

const router = new VueRouter({
  routes,
  mode: "history",
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((route) => route.meta?.requiresAuth)) {
    if (store.state.authorized) {
      next();
    } else {
      next("/auth");
    }
  } else {
    next();
  }
});

export default router;
