import Vuex from "vuex";
import Vue from "vue";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    board: {},
    previewTasks: [],
    authorized: false,
    userName: "",
    userId: "",
    taskMakers: {},
    taskInfo: {},
    boards: [],
  },

  getters: {
    getAuthStatus: ({ authorized }) => authorized,
    getPreviewTasks: ({ previewTasks }) => previewTasks,
    getBoard: ({ board }) => board,
    getTaskInfo: ({ taskInfo }) => taskInfo,
    getTaskMakers: ({ taskMakers }) => taskMakers,
    getBoards: ({ boards }) => boards,
  },

  actions: {
    async getPreviewTasks(context) {
      const tasks = await fetch("http://127.0.0.1:8000/landing").then((data) =>
        data.json()
      );
      context.commit("setPreviewTasks", tasks);
    },
    async getBoard(context, board_id) {
      const board = await fetch(`http://127.0.0.1:8000/board/${board_id}`).then(
        (data) => data.json()
      );
      context.commit("setBoard", board);
    },
    async getTaskMakers(context, path) {
      const info = await fetch(`http://127.0.0.1:8000/board/${path}`).then(
        (data) => data.json()
      );
      context.commit("setTaskMakers", {
        reviewer: info.reviewer,
        assigned: info.assigned,
        author: info.author,
      });
      context.commit("setTaskInfo", { task: info.task });
    },
    async changeTaskStatus(context, { status, path }) {
      let data = { status: status };
      const boards = await fetch(`http://127.0.0.1:8000/board/${path}`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      }).then((data) => data.json());
      context.commit("setBoard", {
        tasks: boards.board.board.tasks,
        users: boards.board.users,
      });
    },
    async changeTaskMakers(context, { role, user, path }) {
      let data = { role: role, user: user };
      const info = await fetch(`http://127.0.0.1:8000/board/${path}`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      }).then((data) => data.json());
      context.commit("setTaskMakers", {
        reviewer: info.reviewer,
        assigned: info.assigned,
        author: info.author,
      });
      context.commit("setTaskInfo", { task: info.task });
      await context.dispatch("getBoard", path.split("/")[0]);
    },
    async auth(context, { username, password }) {
      const user = await fetch("http://127.0.0.1:8000/auth", {
        method: "GET",
        headers: {
          Authorization: "Basic " + btoa(username + ":" + password),
        },
      }).then((data) => data.json());
      if (user) {
        context.commit("setAuth", {
          authorized: true,
          userName: user.username,
          userId: user.id,
          boards: user.boards,
        });
      }
    },
  },
  mutations: {
    setBoard(state, board) {
      state.board = board;
    },
    setPreviewTasks(state, tasks) {
      state.previewTasks = tasks;
    },
    setBoards(state, boards) {
      state.boards = boards;
    },
    setAuth(state, { authorized, userName, boards }) {
      state.authorized = authorized;
      state.userName = userName;
      state.boards = boards;
    },
    setTaskMakers(state, taskMakers) {
      state.taskMakers = taskMakers;
    },
    setTaskInfo(state, taskInfo) {
      state.taskInfo = taskInfo;
    },
  },
});
export default store;
