import Vue from "vue";
import App from "./App.vue";
import router from "@/router";
import store from "@/store";
import VueRouter from "vue-router";
import { BootstrapVue } from "bootstrap-vue";

// Тут можно было бы и не тянуть в бандл весь бутстрап ради одного компонента,
// но планирую накидать еще, поэтому пусть пока будет.

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

Vue.use(BootstrapVue);

Vue.use(VueRouter);
Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: (h) => h(App),
}).$mount("#app");
